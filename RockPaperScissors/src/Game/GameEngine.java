package Game;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class GameEngine {
	/**
	 * All figures who can choose by a player
	 */
	public enum Figure {
		ROCK, PAPER, SCISSORS;

		public static Figure getRandom() {
			return values()[(int) (Math.random() * values().length)];
		}
	}
	/**
	 * Possible results
	 */
	public enum Result {
		WIN, LOOSE, DRAW
	}

	/**
	 * Rules for the game
	 * @param myFigure
	 * @param oponentFigure
	 * @return result
	 */
	private Result rulesEngine(Figure myFigure, Figure oponentFigure) {
		if (myFigure == null) {
			myFigure = Figure.getRandom();
		}
		if (oponentFigure == null) {
			oponentFigure = Figure.getRandom();
		}
		switch (myFigure) {
		case PAPER:
			Set<Figure> paperWinsAgainst = new HashSet<>();
			paperWinsAgainst.add(Figure.ROCK);
			if (paperWinsAgainst.contains(oponentFigure)) {
				return Result.WIN;
			} else if (Figure.PAPER.equals(oponentFigure)) {
				return Result.DRAW;
			} else {
				return Result.LOOSE;
			}
		case ROCK:
			Set<Figure> rockWinsAgainst = new HashSet<>();
			rockWinsAgainst.add(Figure.SCISSORS);
			if (rockWinsAgainst.contains(oponentFigure)) {
				return Result.WIN;
			} else if (Figure.ROCK.equals(oponentFigure)) {
				return Result.DRAW;
			} else {
				return Result.LOOSE;
			}
		case SCISSORS:
			Set<Figure> scissorsWinsAgainst = new HashSet<>();
			scissorsWinsAgainst.add(Figure.PAPER);
			if (scissorsWinsAgainst.contains(oponentFigure)) {
				return Result.WIN;
			} else if (Figure.SCISSORS.equals(oponentFigure)) {
				return Result.DRAW;
			} else {
				return Result.LOOSE;
			}
		default:
			throw new IllegalArgumentException();
		}
	}

	public Result play(Figure myFigure, Figure oponentFigure) {
		Map<Result, Integer> runResults = play(myFigure,oponentFigure,1);
		if(runResults.get(Result.WIN) > 0){
			return Result.WIN;
		}else if(runResults.get(Result.LOOSE) > 0){
			return Result.LOOSE;
		} else if(runResults.get(Result.DRAW) > 0){
			return Result.DRAW;
		}else {
			throw new IllegalArgumentException();
		}
	}
	
	public Map<Result, Integer> play(Figure myFigure, Figure oponentFigure, int iterationsToPlay) {
		Map<Result, Integer> runResults = new HashMap<>();
		int win = 0;
		int loose = 0;
		int draw = 0;

		for (int i = 0; i < iterationsToPlay; i++) {
			switch (rulesEngine(myFigure, oponentFigure)) {
			case DRAW:
				draw++;
				break;
			case LOOSE:
				loose++;
				break;
			case WIN:
				win++;
				break;
			}
		}
		runResults.put(Result.WIN, win);
		runResults.put(Result.LOOSE, loose);
		runResults.put(Result.DRAW, draw);

		return runResults;
	}
}
