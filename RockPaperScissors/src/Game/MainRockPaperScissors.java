package Game;

import java.util.Map;

import Game.GameEngine.Figure;
import Game.GameEngine.Result;

/**
 * rock paper scissors game
 * How to play the game:
 * args: 0 = first Figure 1 = second Figure 2 = iterations
 * Example: rock rock 1
 * Expected result
 * WIN: 0
 * LOOSE: 0
 * Draw: 1
 */
public class MainRockPaperScissors {
	public static void main(String[] args) {
		Figure myFigure = null;
		Figure opponenFigure = null;		
		int interations = 1;
		
		if (args.length >= 3) {
			myFigure = parseArguments(args[0]);
			opponenFigure = parseArguments(args[1]);
			interations = Integer.valueOf(args[2]);
		}

		GameEngine gameEngine = new GameEngine();
		Map<Result, Integer> runResults = gameEngine.play(myFigure, opponenFigure, interations);
		Output output = new Output();
		output.writeGameResult(runResults.get(Result.WIN), runResults.get(Result.LOOSE), runResults.get(Result.DRAW));
	}

	private static Figure parseArguments(String figureName) {
		if ("rock".equals(figureName)) {
			return Figure.ROCK;
		} else if ("paper".equals(figureName)) {
			return Figure.PAPER;
		} else if ("scissors".equals(figureName)) {
			return Figure.SCISSORS;
		}
		return null;
	}
}
