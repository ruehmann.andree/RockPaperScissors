package Game;

/**
 * Output for the game rock paper scissors
 */
public class Output {
	public void writeGameResult(int win, int loose, int draw) {
		System.out.println("WIN: " + win);
		System.out.println("LOOSE: " + loose);
		System.out.println("DRAW: " + draw);
	}
}
