package Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.Test;

import Game.GameEngine;
import Game.MainRockPaperScissors;

public class RockPaperScissorsTests extends GameEngine {

	@Test
	public void testRockVSRock() {
		GameEngine gameEngine = new GameEngine();
		assertEquals(Result.DRAW, gameEngine.play(Figure.ROCK, Figure.ROCK));
	}

	@Test
	public void testRockVSPaper() {
		GameEngine gameEngine = new GameEngine();
		assertEquals(Result.LOOSE, gameEngine.play(Figure.ROCK, Figure.PAPER));
	}

	@Test
	public void testRockVSScissors() {
		GameEngine gameEngine = new GameEngine();
		assertEquals(Result.WIN, gameEngine.play(Figure.ROCK, Figure.SCISSORS));
	}

	@Test
	public void testPaperVSScissors() {
		GameEngine gameEngine = new GameEngine();
		assertEquals(Result.LOOSE, gameEngine.play(Figure.PAPER, Figure.SCISSORS));
	}

	@Test
	public void testRandom() {
		GameEngine gameEngine = new GameEngine();
		Result result = gameEngine.play(null, null);
		assertTrue(result.equals(Result.DRAW) || result.equals(Result.WIN) || result.equals(Result.LOOSE));
	}

	@Test
	public void testIterations() {
		GameEngine gameEngine = new GameEngine();
		Map<Result, Integer> runResults = gameEngine.play(Figure.ROCK, Figure.ROCK, 100);
		assertEquals(100, runResults.get(Result.DRAW).intValue());
	}

	@Test
	public void testRandomIterations() {
		GameEngine gameEngine = new GameEngine();
		Map<Result, Integer> runResults = gameEngine.play(null, Figure.ROCK, 100);
		assertEquals(100, runResults.get(Result.DRAW) + runResults.get(Result.WIN) + runResults.get(Result.LOOSE));
	}

	@SuppressWarnings("static-access")
	@Test
	public void testMainRockPaperScissors() {
		MainRockPaperScissors rockPaperScissors = new MainRockPaperScissors();
		String[] args = { "random", "rock", "100" };
		rockPaperScissors.main(args);
	}
}
